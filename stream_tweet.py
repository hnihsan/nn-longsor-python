from __future__ import absolute_import, print_function
from tools.UBLTextMining import NeuralNetwork
from tweepy import OAuthHandler, Stream, StreamListener
import json
import mysql.connector
from datetime import datetime
# Go to http://apps.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key="Wj62IMs0E3Mk82CqscYBYXKWq"
consumer_secret="v8xWUlFyAwwyrNxnVQP0m9TUJtzZGKr5FL6pMl1D4aWGIzREdI"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token="588890544-NxMMx97OmFl1zvh8GW2Q4ITfZlenk0dhdCruFaG8"
access_token_secret="GSv1oCJrywYDu4EwJJzD9nq3Mr0OTfmmcHhWh8WcdczR9"

class StdOutListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="",
        database="dataset_skripsi"
    )

    cursor = db.cursor()
    def on_data(self, data):
        #print(data)
        # i=1
        # f= open("lib\\mantap.json","w+")
        # f.write(data)
        # print ("Saved {} tweet".format(i))
        # i+= 1
        tweet_data = json.loads(data)
        var_screen_name = tweet_data['user']['screen_name'] if 'user' in tweet_data else ''
        var_name = tweet_data['user']['name'] if 'user' in tweet_data else ''
        var_text = tweet_data['text'] if 'text' in tweet_data != None else ''
        ts = tweet_data['timestamp_ms'] if 'timestamp_ms' in tweet_data else 0
        var_timestamp =datetime.fromtimestamp(int(ts) / 1e3)

        nn = NeuralNetwork()
        classify_result = nn.classify(sentence=var_text)
        classified = {res["class"]: res['score'].item() for res in classify_result}
        classified['predicted'] = '0' if classified["non-warning"] > classified["warning"] else '1'

        self.cursor.execute('''INSERT INTO training_data (username, name, text, created_at, class, score_0, score_1) VALUES(%s, %s, %s, %s, %s, %s, %s )''',(
            var_screen_name,
            var_name,
            var_text,
            var_timestamp,
            classified['predicted'],
            str(classified['non-warning']),
            str(classified['warning'])
        ))
        self.db.commit()
        print (var_text)
        return True

    def on_error(self, status):
        print(status)

if __name__ == '__main__':
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    stream = Stream(auth, l)
    stream.filter(track=['cinta'])