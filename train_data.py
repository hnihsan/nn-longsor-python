from tools.UBLTextMining import NeuralNetwork

nn = NeuralNetwork(data_source = 'lib\\data_training.csv')
nn.start_training_process()