import re
import csv
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

data_tweet = []

with open('lib\\data_training.csv') as csvfile: #load dataset dari csv.
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        data_tweet.append((row[1], row[2]))

with open('lib\\stopwords.txt', 'r') as f: #load stop words
    stop_words = f.read().split('\n')

stemmed_data=[]

# for item in data_tweet:
#     sentence = item[0]
#
#     factory = StemmerFactory()
#     stemmer = factory.create_stemmer()
#
#     # CASE FOLDING
#     kalimat = sentence.lower()
#
#     # TOKENIZATION
#     kalimat = re.sub('[^A-Za-z0-9# ]+', '', kalimat)
#     kalimat = kalimat.split()
#
#     # STOPWORD REMOVAL
#     for word in kalimat:
#         if word in stop_words:
#             kalimat.remove(word)
#
#     # STEMMING SASTRAWI
#     stemmed = []
#     for word in kalimat:
#         stemmed.append(stemmer.stem(word))
#
#     stemmed_data.append(stemmed)
#
#
# sentence = data_tweet[0][0]
sentence = "11 tewas san 7 orang hilang dari longsor di Brebes. 5 alat berat, 400 personil tim SAR gabungan dan anjing pelacak dikerahkan mencari korban. Hujan menghambat pencarian korban. Gotong royong dan semangat tim SAR gabungan modal sosial yang kuat mengatasi bencana. pic.twitter.com/8s2atyKWS9"

factory = StemmerFactory()
stemmer = factory.create_stemmer()

# CASE FOLDING
kalimat = sentence.lower()

# TOKENIZATION
kalimat = re.sub('[^A-Za-z0-9# ]+', '', kalimat)
kalimat = kalimat.split()

# STOPWORD REMOVAL
for word in kalimat:
    if word in stop_words:
        kalimat.remove(word)

# STEMMING SASTRAWI
stemmed = []
for word in kalimat:
    stemmed.append(stemmer.stem(word))

stemmed_data.append(stemmed)

print(stemmed_data[0])
