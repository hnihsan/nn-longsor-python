from tools.UBLTextMining import NeuralNetwork
import csv
import json

nn = NeuralNetwork()
katadasar_dict = {}
with open('lib\\corpus_new.txt') as f:
    corpus_indo = f.read().split()

for word in corpus_indo:
    katadasar_dict[word] = 0

training_data = []
with open('lib\\longsor_raw_data.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        training_data.append(row[2])

len_training = len(training_data)
i = 1

for kalimat in training_data:
    kalimat_tokenize = nn.preprocess_sentence_only(sentence=kalimat)
    for kata in kalimat_tokenize:
        if kata in katadasar_dict:
            x = katadasar_dict[kata]
            katadasar_dict[kata] = x + 1
    print('Loading {}%'.format(i / len_training * 100))
    i = i + 1

katadasar_score_file = "source\\katadasar_score.json"
with open(katadasar_score_file, 'w') as outfile:
    json.dump(katadasar_dict, outfile, indent=4, sort_keys=True)
print ("saved score to:", katadasar_score_file)

sorted_katadasar = sorted(katadasar_dict, key=katadasar_dict.get, reverse=True)

i = 0
with open('lib\\input_layer.txt', 'w') as f:
    for item in sorted_katadasar:
        if i <= 200:
          f.write("%s\n" % item)
          i = i +1
        else:
            break

print ("Input Layer Listed in.")

# Loop tiap record buat dapetin jumlah tiap word increment
# Sort dict
# Write ke JSON