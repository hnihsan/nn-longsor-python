import csv
import json
import mysql.connector
from datetime import datetime
import re
training_data = []
with open('lib\\data_training.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        training_data.append(row)

db = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="",
        database="dataset_skripsi"
    )

cursor = db.cursor()
for item in training_data:

    tweet_data = item
    var_screen_name = re.sub('[^A-Za-z0-9 ]+', '', tweet_data[0])
    var_name = var_screen_name
    var_text = tweet_data[1]
    var_timestamp = datetime.fromtimestamp(1562640498)
    var_class = tweet_data[2]
    cursor.execute('''INSERT INTO training_data (username, name, text, created_at, class) VALUES(%s, %s, %s, %s, %s )''',(
        var_screen_name,
        var_name,
        var_text,
        var_timestamp,
        var_class
    ))
    db.commit()

print("Done")