from UBLTextMining import NeuralNetwork
import csv
import json
import math

nn = NeuralNetwork()
katadasar_dict = {}
with open('lib\\corpus_new.txt') as f:
    corpus_indo = f.read().split()

for word in corpus_indo:
    katadasar_dict[word] = 0

training_data = []
with open('lib\\longsor_raw_data.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        training_data.append(row[2])

total_terms = len(corpus_indo)
total_training_data = len(training_data)

len_training = len(training_data)
i = 1

term_count_dict = katadasar_dict
for kalimat in training_data:
    kalimat_tokenize = nn.preprocess_sentence_only(sentence=kalimat)
    for kata in kalimat_tokenize:
        if kata in term_count_dict:
            x = term_count_dict[kata]
            term_count_dict[kata] = x + 1
    print('Loading Counting TF{}%'.format(i / len_training * 100))
    i = i + 1

tf_terms={}
for index in term_count_dict:
    tf_terms[index] = term_count_dict[index]/total_terms

idf_terms={}
term_in_doc=katadasar_dict
for tweet in training_data:
    tweet_tokenize = nn.preprocess_sentence_only(sentence=tweet)
    for word in term_in_doc:
        if word in tweet_tokenize:
            x = term_in_doc[word]
            term_in_doc[word] = x + 1
            break

    print('Loading Counting df {}%'.format(i / len_training * 100))
    i = i + 1

for word in term_in_doc:
    idf_terms[word] = math.log((total_training_data/term_in_doc[word]))

tfidf_terms = katadasar_dict
for word in tfidf_terms:
    tfidf_terms[word] = tf_terms[word] * idf_terms[word]

tfidf_score_file = "source\\katadasar_score_tfidf.json"
with open(tfidf_score_file, 'w') as outfile:
    json.dump(tfidf_terms, outfile, indent=4, sort_keys=True)
print ("saved score to:", tfidf_score_file)

sorted_tfidf = sorted(tfidf_terms, key=tfidf_terms.get, reverse=True)

i = 0
with open('lib\\input_layer_tfidf.txt', 'w') as f:
    for item in sorted_tfidf:
        if i <= 200:
          f.write("%s\n" % item)
          i = i +1
        else:
            break

print ("Input Layer Listed in.")

# Loop tiap record buat dapetin jumlah tiap word increment
# Sort dict
# Write ke JSON