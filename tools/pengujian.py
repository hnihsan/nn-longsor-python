from UBLTextMining import NeuralNetwork
import csv
import numpy as np
import json

nn = NeuralNetwork(data_source='lib\\data_testing.csv')

# Load Data from process (take long time)
# testing_data = nn.start_mse_process()

# Syntax to save to file
# saved_testing_data = 'lib\\testing\\mse_source.json'
# with open(saved_testing_data, 'w') as outfile:
#     json.dump(testing_data, outfile, indent=4, sort_keys=False)
# print ("saved synapses to:", saved_testing_data)

# Load data from json
# with open('lib\\testing\\mse_source.json') as data_file:
#     testing_data = json.load(data_file)
#
# print("Target : ",testing_data[1]['target'])

testing_accuracy = nn.start_hitung_akurasi()
# Syntax to save to file
saved_testing_accuracy = 'lib\\testing\\accuracy_source.json'
with open(saved_testing_accuracy , 'w') as outfile:
    json.dump(testing_accuracy, outfile, indent=4, sort_keys=False)
print ("saved synapses to:", saved_testing_accuracy )
print(testing_accuracy[0])
