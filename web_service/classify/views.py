import json
from flask import request, jsonify, Blueprint, abort
from flask.views import MethodView
from web_service import app
from web_service.classify.models import ClassifyProcedure


classifyprocedure = Blueprint('classifyprocedure', __name__)

@classifyprocedure.route("/update_score_all", methods=["GET"])
def update_score_all():
    result = ClassifyProcedure.update_score(app)
    return jsonify(result)

@classifyprocedure.route("/classify/<id>", methods=["GET"])
def classifyDataset(id):
    result = ClassifyProcedure.classifyDataset(app,id=id)
    return jsonify(result)

class ClassifyView(MethodView):

    def post(self):
        text = request.form.get('text')

        result = ClassifyProcedure.classifyText(app, text)
        return jsonify({"data":result})


classify_view = ClassifyView.as_view('classify_view')
app.add_url_rule(
    '/classify', view_func=classify_view, methods=['POST']
)