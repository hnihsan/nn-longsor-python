from web_service import db
from tools.UBLTextMining import NeuralNetwork

class ClassifyProcedure:

    def classifyText(self, text):
        nn = NeuralNetwork()
        preprocessed_text = ""
        for item in nn.preprocess_sentence_only(text):
            preprocessed_text += (item + " ")
        classify_result = nn.classify(sentence=text)
        classified = {res["class"]: res['score'].item() for res in classify_result}
        classified['predicted'] = '0' if classified["non-warning"] > classified["warning"] else '1'
        return {"original":text,"preprocessed":preprocessed_text,"class":classified}

    def classifyDataset(self, id):
        conn = db.connect()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT * FROM training_data WHERE id = %s",(id)
        )
        result = cursor.fetchall()
        tweet = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]

        nn = NeuralNetwork()
        preprocessed_text = ""
        for item in nn.preprocess_sentence_only(tweet[0]["text"]):
            preprocessed_text += (item + " ")
        classify_result = nn.classify(sentence=tweet[0]["text"])
        classified = {res["class"]: res['score'].item() for res in classify_result}
        classified['predicted'] = '0' if classified["non-warning"] > classified["warning"] else '1'
        cursor.execute(
            '''UPDATE training_data set class=%s,score_0=%s, score_1=%s WHERE id=%s''', (
                classified['predicted'],
                str(classified['non-warning']),
                str(classified['warning']),
                id
            ))
        conn.commit()
        cursor.close()
        return {"original": tweet[0]["text"], "preprocessed": preprocessed_text, "class": classified}

    def update_score(self):
        conn = db.connect()
        cursor = conn.cursor()
        nn = NeuralNetwork()
        cursor.execute("SELECT * FROM training_data ")
        # cursor.execute("SELECT * FROM training_data WHERE text like '%longsor%'")
        result = cursor.fetchall()
        cursor.close()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]

        debug_classified=[]
        for data in data_train:
            classify_result = nn.classify(sentence=data['text'])
            # classified= {res['class']:{"class_id":res['class_id'], "score":res['score'].item()} for res in classify_result}
            classified= {res["class"]:res['score'].item() for res in classify_result}
            classified['predicted'] = '0' if classified["non-warning"] > classified["warning"] else '1'
            # debug_classified.append(classified)
            # score_0 = classified['non-warning']['score']
            # score_1 = classified['warning']['score']
            #
            # predicted_class = classified['non-warning']['class_id'] if score_0 > score_1 else classified['warning']['class_id']
            conn = db.connect()
            cursor = conn.cursor()
            cursor.execute(
                '''UPDATE training_data set class=%s,score_0=%s, score_1=%s WHERE id=%s''', (
                    classified['predicted'],
                    str(classified['non-warning']),
                    str(classified['warning']),
                    data['id']
                ))
            conn.commit()
            cursor.close()
        return {"status":"Berhasil"}
        # return {"data":debug_classified}