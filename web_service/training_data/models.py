from web_service import db
from tools.UBLTextMining import NeuralNetwork
class DataTraining():

    def all(self, page=1):
        conn = db.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM training_data ORDER BY created_at DESC")
        result = cursor.fetchall()
        data_train = [({cursor.description[i][0]:item[i] for i in range(len(cursor.description))}) for item in result]

        return data_train

    def today(self):
        conn = db.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM training_data WHERE DATE_FORMAT(created_at, '%Y-%m-%d') = DATE_FORMAT(sysdate(), '%Y-%m-%d') ORDER BY created_at DESC")
        result = cursor.fetchall()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        cursor.close()
        return data_train

    def find(self, id):
        conn = db.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM training_data where id=%s ORDER BY created_at DESC"%(id))
        result = cursor.fetchall()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        cursor.close()
        return data_train

    def search(self, keyword):
        conn = db.connect()
        cursor = conn.cursor()
        search_keyword = "%"+keyword+"%"
        cursor.execute("SELECT * FROM training_data WHERE text LIKE %s OR name like %s OR username like %s OR created_at like %s ORDER BY created_at DESC",(search_keyword, search_keyword, search_keyword, search_keyword))
        result = cursor.fetchall()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        cursor.close()
        return data_train

    def insert(self, username, name, text):
        conn = db.connect()
        cursor = conn.cursor()
        status = cursor.execute("INSERT INTO training_data (username,name,text) VALUES(%s, %s, %s)",(username, name, text))
        if status:
            conn.commit()
            return True
        else:
            return False

    def update(self, id, username, name, text, kelas):
        conn = db.connect()
        cursor = conn.cursor()
        status = cursor.execute("UPDATE training_data SET username=%s, name=%s, text=%s, class=%s WHERE id=%s",(username, name, text, kelas, id))
        if status:
            conn.commit()
            cursor.close()
            return True
        else:
            return False

    def delete(self, id):
        conn = db.connect()
        cursor = conn.cursor()
        status = cursor.execute("DELETE FROM training_data WHERE id=%s",(id))
        if status:
            conn.commit()
            cursor.close()
            return True
        else:
            return False

    def statistikAll(self):
        conn = db.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT class as class_id, count(*) as total FROM training_data GROUP BY class")
        result = cursor.fetchall()
        cursor.close()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        statistik = {}
        for item in data_train:
            if item["class_id"] == '0':
                label = "nonwarning"
            elif item["class_id"] == '1':
                label = "warning"
            else:
                label = "unclassified"
            statistik[label] = item["total"]
        # for item in data_train:
        #     if item['class_id'] is '':
        #         class_name='Unclassified'
        #     else:
        #         class_name=NeuralNetwork.classes[int(item['class_id'])]
        #     data_train['class_name'] = class_name
        return statistik

    def statistikSearch(self, keyword):
        conn = db.connect()
        cursor = conn.cursor()
        search_keyword = "%" + keyword + "%"
        cursor.execute("SELECT class as class_id, count(*) as total FROM training_data WHERE text LIKE %s OR name like %s OR username like %s OR created_at like %s GROUP BY class",
                       (search_keyword, search_keyword, search_keyword, search_keyword))
        result = cursor.fetchall()
        cursor.close()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        statistik = {}
        for item in data_train:
            if item["class_id"] == '0':
                label = "nonwarning"
            elif item["class_id"] == '1':
                label = "warning"
            else:
                label = "unclassified"
            statistik[label] = item["total"]
        # for item in data_train:
        #     if item['class_id'] is '':
        #         class_name='Unclassified'
        #     else:
        #         class_name=NeuralNetwork.classes[int(item['class_id'])]
        #     data_train['class_name'] = class_name
        return statistik

    def total(self):
        conn = db.connect()
        cursor = conn.cursor()
        cursor.execute(
            "SELECT COUNT(0) as total FROM training_data"
        )
        result = cursor.fetchall()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        cursor.close()
        return data_train

    def total_bySearch(self, keyword):
        conn = db.connect()
        cursor = conn.cursor()
        search_keyword = "%" + keyword + "%"
        cursor.execute(
            "SELECT COUNT(0) as total FROM training_data WHERE text LIKE %s OR name like %s OR username like %s OR created_at like %s ORDER BY created_at DESC",(search_keyword, search_keyword, search_keyword, search_keyword))
        result = cursor.fetchall()
        data_train = [({cursor.description[i][0]: item[i] for i in range(len(cursor.description))}) for item in result]
        cursor.close()
        return data_train