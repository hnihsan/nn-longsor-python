import json
from flask import request, jsonify, Blueprint, abort
from flask.views import MethodView
from web_service import app
from web_service.training_data.models import DataTraining

datatraining = Blueprint('datatraining', __name__)


@datatraining.route('/')
@datatraining.route('/home')
def home():
    return "Welcome to the Data Home."
@datatraining.route('/data/today')
def today():
    data_training = DataTraining.today(app)
    return jsonify(data_training)

@datatraining.route('/data/statistik/')
def statistikAll():
    statistik = DataTraining.statistikAll(app)
    return jsonify(statistik)

@datatraining.route('/data/statistik/<keyword>')
def statistikSearch(keyword=None):
    statistik = DataTraining.statistikSearch(app,keyword=keyword)
    return jsonify(statistik)

@datatraining.route("/data/total/", methods=["GET"])
def getTotal():
    result = DataTraining.total(app)
    return jsonify(result[0])

@datatraining.route("/data/total/<keyword>", methods=["GET"])
def getTotalBySearch(keyword):
    result = DataTraining.total_bySearch(app, keyword=keyword)
    return jsonify(result[0])

class DataTrainingView(MethodView):

    def get(self, id=None, keyword=None, page=1):
        if not id and not keyword:
            data_training = DataTraining.all(app)
        elif not id:
            data_training = DataTraining.search(app, keyword)
        else:
            data_training = DataTraining.find(app, id)

        return jsonify({"data":data_training})
        # return jsonify(data_training)

    def post(self):
        username = request.form.get('username')
        name = request.form.get('name')
        text = request.form.get('text')

        if DataTraining.insert(app, username, name, text):
            return jsonify(
                {"status" : "Ok"}
            )
        else:
            return jsonify(
                {"status": "Failed"}
            )

    def put(self, id):
        username = request.form.get('username')
        name = request.form.get('name')
        text = request.form.get('text')
        kelas= request.form.get('class')

        if DataTraining.update(app, id=id, username=username, name=name, text=text, kelas=kelas):
            return jsonify(
                {"status": "Ok"}
            )
        else:
            return jsonify(
                {"status": "Failed"}
            )

    def delete(self, id):
        if DataTraining.delete(app, id=id):
            return jsonify(
                {"status": "Ok"}
            )
        else:
            return jsonify(
                {"status": "Failed"}
            )



datatraining_view = DataTrainingView.as_view('datatraining_view')
app.add_url_rule(
    '/data/', view_func=datatraining_view, methods=['GET', 'POST']
)
app.add_url_rule(
    '/data/<int:id>', view_func=datatraining_view, methods=['GET', 'PUT', 'DELETE']
)
app.add_url_rule(
    '/data/search/<keyword>', view_func=datatraining_view, methods=['GET']
)