from flask import Flask
from flaskext.mysql import MySQL
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
db = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'dataset_skripsi'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
db.init_app(app)



from web_service.training_data.views import datatraining
from web_service.classify.views import classifyprocedure
app.register_blueprint(datatraining)
app.register_blueprint(classifyprocedure)
