from tools.UBLTextMining import NeuralNetwork

nn = NeuralNetwork()
nn.load_corpus(data_source='dataset\\input_layer.txt')
nn.load_stop_words(data_source='dataset\\stopwords.txt')
nn.load_cf_data(data_source='dataset\\gempa.csv')

binary_dataset = nn.preprocess_sentence_independent()

score_temp = nn.start_hitung_akurasi_nilai_only(binary_dataset) #pake synapses nya buat prediksi
score_file = open("result\\score_testing.txt", "w")
score_file.write("Accuracy Test" + ";" + str(score_temp) + "\n")

print("Done")
