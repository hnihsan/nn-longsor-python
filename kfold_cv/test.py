# scikit-learn k-fold cross-validation
import csv
from numpy import array
from sklearn.model_selection import KFold
# data sample
#
dataset = []
with open('dataset\\gempa.csv') as csvfile:
	readCSV = csv.reader(csvfile, delimiter=';')
	for row in readCSV:
		dataset.append((row[0], row[1]))

# dataset = array([0.1, 0.2, 0.3, 0.4, 0.5, 0.6])
# prepare cross validation

print(type(dataset))
# kfold = KFold(3, True, 1)
# # enumerate splits
# splitted_data = kfold.split(dataset) #train and test
# with open('dataset\\test\\test_kfold.txt', 'w') as f:
# 	for train, test in splitted_data:
# 		for item in dataset[test]:
# 			f.write("%s " % item[1])
# 		f.write("\n")
#
# # print (dataset[1])
# print('done')
