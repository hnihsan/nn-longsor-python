from tools.MNBTextMining import NaiveBayes
import csv
import random

nb = NaiveBayes()

def get_data(dataset):
    # subfolders = ['enron%d' % i for i in range(1,7)]
    data = []
    target = []
    for row in dataset:
        data.append(row[0])
        target.append(int(row[1]))
    return data, target

shuffled_dataset = []
with open('dataset\\gempa.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    dataset = []
    for row in readCSV:
        dataset.append(row)
    shuffled_dataset = random.sample(dataset, len(dataset))

component_size = int(len(shuffled_dataset) / 10)

dataset = {}
for i in range(10) : # use 10-fold
    start = i*component_size
    end = (i+1)*component_size
    # print(start,end)
    dataset[i] = shuffled_dataset[int(start) : int(end)]

score_file = open("result\\kfold_cv_score_nb.txt", "a")
for i in range(10):
    testing_X, testing_y = get_data(dataset[i])
    training_X = []
    training_y = []
    for j in range(10):
        if(i != j):
            temp_X, temp_y = get_data(dataset[j])
            training_X += temp_X
            training_y += temp_y

    nb.fit(training_X, training_y)
    pred = nb.predict(testing_X)
    accuracy = sum(1 for i in range(len(pred)) if pred[i] == testing_y[i]) / float(len(pred))
    score_file.write("Accuracy Fold " + str(i+1)+ ";" + str(accuracy * 100) + "\n")
score_file.close()

print("Done")


