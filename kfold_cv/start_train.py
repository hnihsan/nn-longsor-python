from tools.UBLTextMining import NeuralNetwork
import random

nn = NeuralNetwork()
nn.load_corpus(data_source='dataset\\input_layer.txt')
nn.load_stop_words(data_source='dataset\\stopwords.txt')
nn.load_cf_data(data_source='dataset\\gempa.csv')

# print(len(nn.training_sentences))
# list_1 = [1,2,3,4,5,6,7,8,9,10]
binary_dataset = nn.preprocess_sentence_independent()
component_size = int(len(binary_dataset) / 10)
dataset = {}
for i in range(10) : # use 10-fold
    start = i*component_size
    end = (i+1)*component_size
    # print(start,end)
    dataset[i] = binary_dataset[int(start) : int(end)]

score_file = open("result\\kfold_cv_score.txt", "a")
for i in range(10):
    # nn.training_sentences = []
    # nn.testing_sentences = dataset[i]
    nn.X = []
    nn.y = []
    for j in range(10):
        if(i != j):
            for data in dataset[j]:
                nn.X.append(data[0])
                nn.y.append(data[1])

    nn.train() #generate synapses
    score_temp = nn.start_hitung_akurasi_nilai_only(dataset[i]) #pake synapses nya buat prediksi
    score_file.write("Accuracy Fold " + str(i+1)+ ";" + str(score_temp) + "\n")
score_file.close()

print("Done")
