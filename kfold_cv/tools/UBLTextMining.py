import re
import csv
from nltk.tokenize import TweetTokenizer
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import numpy as np
import datetime
import json
import random

class NeuralNetwork:
    corpus = [] #input layer
    kbbi = []
    classes = ['nonwarning','warning']
    training_sentences = []
    testing_sentences = []
    shuffled_dataset = []
    stop_words = []

    X = []
    y = []

    # def __init__ (self, data_source = 'lib\\data_training.csv', corpus_source='lib\\input_layer.txt',stopwords_source='lib\\stopwords.txt', input_classes = []):
    #     if input_classes :
    #         self.classes = input_classes
    #     self.load_training_data(data_source)
    #     self.load_corpus(corpus_source)
    #     self.load_kbbi()
    #     self.load_stop_words(stopwords_source)
#  -------------------- Main Function --------------------
    def start_training_process(self):
        self.preprocess_sentence()
        self.train()

    def start_mse_process(self):
        testing_data = []
        i=1
        training_sentences_temp = self.training_sentences
        for doc in training_sentences_temp:
            tweet_data = {}
            classes_in_binary = [0] * len(self.classes)
            classes_in_binary[int(doc[1])] = 1
            classes_to_compare = np.array(classes_in_binary)

            prediction = self.predict(sentence=doc[0])
            prediction_list = prediction.tolist()
            layer_2_error = np.array(classes_to_compare - prediction)
            last_mean_error = np.mean(np.abs(layer_2_error))
            mse = np.square(last_mean_error)

            tweet_data['content'] = doc[0]
            tweet_data['target_0']=classes_in_binary[0]
            tweet_data['target_1']=classes_in_binary[1]
            tweet_data['prediction_0']=prediction_list[0]
            tweet_data['prediction_1']=prediction_list[1]
            tweet_data['mse'] = mse
            testing_data.append(tweet_data)
            progress = i/len(training_sentences_temp) * 100
            i+=1
            print('Progress : ', progress, ' %')

        return testing_data

    def start_hitung_akurasi(self):
        testing_data = []
        i = 1

        training_sentences_temp = self.training_sentences[:500]
        for doc in training_sentences_temp:
            tweet_data = {}

            prediction = self.predict(sentence=doc[0])
            label_prediksi = 1 if prediction[1] > prediction[0] else 0

            tweet_data['content'] = doc[0]
            tweet_data['target'] = 'Warning' if int(doc[1]) == 1 else 'Non-Warning'
            tweet_data['prediksi'] = 'Warning' if label_prediksi == 1 else 'Non-Warning'
            if label_prediksi == int(doc[1]):
                tweet_data['status'] = 'Cocok'
                tweet_data['true'] = 1
            else:
                tweet_data['status'] = 'Tidak Cocok'
                tweet_data['true'] = 0

            tweet_data['total_data'] = len(training_sentences_temp)

            testing_data.append(tweet_data)
            progress = i / len(training_sentences_temp) * 100
            i += 1
            print('Progress : ', progress, ' %')
        return testing_data

    def start_hitung_akurasi_nilai_only(self, testing_data):
        correct_score = 0
        i = 1

        for doc in testing_data:
            prediction = self.predict_no_bow(binary_x=doc[0])
            label_prediksi = 1 if prediction[1] > prediction[0] else 0
            label_test = 1 if doc[1][1] > doc[1][0] else 0
            print("Prediction : ", label_prediksi, " Result : ", label_test)
            correct_score += (1 if label_prediksi == label_test else 0)

            progress = i / len(testing_data) * 100
            i += 1
            print('Progress Prediction: ', progress, ' %')
        final_score = correct_score / len(testing_data) * 100
        print("Jumlah Benar : ", correct_score, " | Jumlah total : ", len(testing_data), " | Final Score : ", final_score)
        return final_score

# -------------------- Single Function --------------------
    def load_training_data(self, data_source = 'lib\\data_training.csv'):
        with open(data_source) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=';')
            for row in readCSV:
                self.training_sentences.append((row[0],row[1]))


    def load_cf_data(self, data_source = 'dataset\\gempa.csv'):
        with open(data_source) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=';')
            dataset = []
            for row in readCSV:
                dataset.append(row)
        self.shuffled_dataset = random.sample(dataset, len(dataset))

    def load_corpus(self, data_source = 'lib\\corpus.txt'):
        with open(data_source) as f:
            self.corpus = f.read().split('\n')

    def load_kbbi(self, data_source = 'lib\\corpus.txt'):
        with open(data_source) as f:
            self.kbbi = f.read().split('\n')

    def load_stop_words(self, data_source = 'lib\\stopwords.txt'):
        with open(data_source, 'r') as f:
            self.stop_words = f.read().split('\n')

    def bag_of_binary_words(self, sentence, show_details=False):
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()

        # CASE FOLDING
        kalimat = sentence.lower()

        # TOKENIZATION
        kalimat = re.sub('[^A-Za-z0-9# ]+', '', kalimat)
        kalimat = kalimat.split()

        # STOPWORD REMOVAL
        for word in kalimat:
            if word in self.stop_words:
                kalimat.remove(word)

        # STEMMING SASTRAWI
        stemmed = []
        for word in kalimat:
            stemmed.append(stemmer.stem(word))
        #return np.array(stemmed)

        words_in_binary = []
        for word in self.corpus:
            if word in stemmed:
                words_in_binary.append(1)
                if(show_details):
                    print ("Found keyword: {}".format(word))
            else:
                words_in_binary.append(0)
        return np.array(words_in_binary)

    def preprocess_sentence_only(self, sentence):
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()

        # CASE FOLDING
        kalimat = sentence.lower()

        # TOKENIZATION
        kalimat = re.sub('[^A-Za-z0-9# ]+', '', kalimat)
        kalimat = kalimat.split()

        # STOPWORD REMOVAL
        for word in kalimat:
            if word in self.stop_words:
                kalimat.remove(word)

        # STEMMING SASTRAWI
        stemmed = []
        for word in kalimat:
            stemmed.append(stemmer.stem(word))

        return np.array(stemmed)

    def preprocess_sentence(self):
        i =1
        progress = 0
        for doc in self.training_sentences:
            progress = i / len(self.training_sentences) * 100
            if(progress % 10 == 0):
                print("Preprocessing progress : "+str(progress))
            self.X.append(self.bag_of_binary_words(sentence=doc[0]))

            classes_in_binary = [0] * len(self.classes)
            classes_in_binary[int(doc[1])] = 1

            self.y.append(classes_in_binary)
            i+=1

    def preprocess_sentence_independent(self):
        i =1
        temp_binary_dataset = []
        for doc in self.shuffled_dataset:
            progress = i / len(self.shuffled_dataset) * 100
            print("Preprocessing progress : {0:.2f}".format(progress))
            temp_X = self.bag_of_binary_words(sentence=doc[0])

            classes_in_binary = [0] * len(self.classes)
            classes_in_binary[int(doc[1])] = 1
            temp_y = classes_in_binary

            temp_set = [temp_X, temp_y]
            temp_binary_dataset.append(temp_set)
            i+=1
        return temp_binary_dataset
    # compute sigmoid nonlinearity
    def sigmoid(self, x):
        output = 1/(1+np.exp(-x))
        return output

    # convert output of sigmoid function to its derivative
    def sigmoid_output_to_derivative(self, output):
        return output*(1-output)

    def predict(self, sentence, show_details=False):
        with open("dataset\\synapses.json") as data_file:
            synapse = json.load(data_file)
            synapse_0 = np.asarray(synapse['synapse0'])
            synapse_1 = np.asarray(synapse['synapse1'])
        x = self.bag_of_binary_words(sentence=sentence, show_details=show_details)
        if show_details:
            print ("sentence:", sentence, "\n bow:", x)
        # input layer is our bag of words
        l0 = x
        # matrix multiplication of input and hidden layer
        l1 = self.sigmoid(np.dot(l0, synapse_0))
        # print("Layer Hidden 1 : ")
        # print(l1)
        # output layer
        l2 = self.sigmoid(np.dot(l1, synapse_1))
        return l2

    def predict_no_bow(self, binary_x, show_details=False):
        with open("dataset\\synapses.json") as data_file:
            synapse = json.load(data_file)
            synapse_0 = np.asarray(synapse['synapse0'])
            synapse_1 = np.asarray(synapse['synapse1'])
        x = binary_x
        if show_details:
            print ("sentence:", sentence, "\n bow:", x)
        # input layer is our bag of words
        l0 = x
        # matrix multiplication of input and hidden layer
        l1 = self.sigmoid(np.dot(l0, synapse_0))
        # print("Layer Hidden 1 : ")
        # print(l1)
        # output layer
        l2 = self.sigmoid(np.dot(l1, synapse_1))
        return l2

    def train(self,hidden_neurons=10, alpha=1, epochs=50000, dropout=False, dropout_percent=0.5 ):
        print ("Training with %s neurons, alpha:%s, dropout:%s %s" % (hidden_neurons, str(alpha), dropout, dropout_percent if dropout else ''))
        print ("Input matrix: %sx%s    Output matrix: %sx%s" % (len(self.X), len(self.corpus), 1, len(self.classes)))

        last_mean_error = 1
        # randomly initialize our weights with mean 0
        synapse_0 = 2 * np.random.random((len(self.corpus), hidden_neurons)) - 1
        synapse_1 = 2 * np.random.random((hidden_neurons, len(self.classes))) - 1

        prev_synapse_0_weight_update = np.zeros_like(synapse_0)
        prev_synapse_1_weight_update = np.zeros_like(synapse_1)

        synapse_0_direction_count = np.zeros_like(synapse_0)
        synapse_1_direction_count = np.zeros_like(synapse_1)

        i = 0
        for j in iter(range(epochs + 1)):

            # Feed forward through layers 0, 1, and 2
            layer_0 = np.array(self.X)
            layer_1 = self.sigmoid(np.dot(layer_0, synapse_0))

            if (dropout):
                layer_1 *= np.random.binomial([np.ones((len(self.X), hidden_neurons))], 1 - dropout_percent)[0] * (
                            1.0 / (1 - dropout_percent))

            layer_2 = self.sigmoid(np.dot(layer_1, synapse_1))

            # how much did we miss the target value?
            layer_2_error = self.y - layer_2

            if (j % 10000) == 0 and j > 5000:
                # if this 10k iteration's error is greater than the last iteration, break out
                if np.mean(np.abs(layer_2_error)) < last_mean_error:
                    print ("delta after " + str(j) + " iterations:" + str(np.mean(np.abs(layer_2_error))))
                    last_mean_error = np.mean(np.abs(layer_2_error))
                else:
                    print ("break:", np.mean(np.abs(layer_2_error)), ">", last_mean_error)
                    break

            # in what direction is the target value?
            # were we really sure? if so, don't change too much.
            layer_2_delta = layer_2_error * self.sigmoid_output_to_derivative(layer_2)

            # how much did each l1 value contribute to the l2 error (according to the weights)?
            layer_1_error = layer_2_delta.dot(synapse_1.T)

            # in what direction is the target l1?
            # were we really sure? if so, don't change too much.
            layer_1_delta = layer_1_error * self.sigmoid_output_to_derivative(layer_1)

            synapse_1_weight_update = (layer_1.T.dot(layer_2_delta))
            synapse_0_weight_update = (layer_0.T.dot(layer_1_delta))
            #print('update:',synapse_1_weight_update, synapse_0_weight_update)
            # print(synapse_1_weight_update)

            if (j > 0):
                synapse_0_direction_count += np.abs(
                    ((synapse_0_weight_update > 0) + 0) - ((prev_synapse_0_weight_update > 0) + 0))
                synapse_1_direction_count += np.abs(
                    ((synapse_1_weight_update > 0) + 0) - ((prev_synapse_1_weight_update > 0) + 0))

            synapse_1 += alpha * synapse_1_weight_update
            synapse_0 += alpha * synapse_0_weight_update

            prev_synapse_0_weight_update = synapse_0_weight_update
            prev_synapse_1_weight_update = synapse_1_weight_update

            # print("Progress : {} %".format(i/epochs*100))
            # i+=1

        now = datetime.datetime.now()

        # persist synapses
        synapse = {'synapse0': synapse_0.tolist(), 'synapse1': synapse_1.tolist(),
                   'datetime': now.strftime("%Y-%m-%d %H:%M"),
                   'classes': self.classes
                   }
        synapse_file = "dataset\\synapses.json"

        with open(synapse_file, 'w') as outfile:
            json.dump(synapse, outfile, indent=4, sort_keys=True)
        print ("saved synapses to:", synapse_file)

    def test_json(self):
        # persist synapses
        now = datetime.datetime.now()
        synapse = {
                   'datetime': now.strftime("%Y-%m-%d %H:%M"),
                   'classes': self.classes
                   }
        synapse_file = "dataset\\synapses.json"

        with open(synapse_file, 'w') as outfile:
            json.dump(synapse, outfile, indent=4, sort_keys=True)
        print ("saved synapses to:", synapse_file)

    def classify(self, sentence, ERROR_THRESHOLD = 0, show_details=False):
        results = self.predict(sentence, show_details)

        results = [[i, r] for i, r in enumerate(results) if r > ERROR_THRESHOLD]
        #print(results)
        results.sort(key=lambda x: x[1],
                     reverse=True)  # fungsi lambda tersebut, maksudnya jadikan index 1 sebagai patokan sortnya
        # return_results = [[self.classes[r[0]], r[1]] for r in results]
        return_results = [{"class_id":r[0],"class": self.classes[r[0]], "score": r[1]} for r in results]
        #print ("%s \n classification: %s" % (sentence, return_results))
        return return_results
