import os
import re
import string
import math
import csv
import datetime
# DATA_DIR = 'enron'
# target_names = ['ham', 'spam']

class NaiveBayes:
    """Implementation of Naive Bayes for binary classification"""
    def testFun(self):
        return "mantap"

    def clean(self, s):
        translator = str.maketrans("", "", string.punctuation)#menghilangkan tanda baca (@,.*&)(&%)
        return s.translate(translator)

    def tokenize(self, text):
        text = self.clean(text).lower() #membuat kalimat semuanya menjadi kecil
        return re.split("\W+", text)

    def tokenizeInString(self, text):
        text = self.clean(text).lower() #membuat kalimat semuanya menjadi kecil
        return text

    def get_word_counts(self, words):
        word_counts = {}
        for word in words:
            word_counts[word] = word_counts.get(word, 0.0) + 1.0 #untuk menampung teks processing dalam bentuk array
        return word_counts

    def fit(self, X, Y):
        self.num_messages = {}
        self.log_class_priors = {}
        self.word_counts = {}
        self.vocab = set()

        n = len(X)
        self.num_messages['warning'] = sum(1 for label in Y if label == 1)
        self.num_messages['non_warning'] = sum(1 for label in Y if label == 0)
        print(self.num_messages)
        print(n)
        self.log_class_priors['warning'] = math.log(self.num_messages['warning'] / n)
        self.log_class_priors['non_warning'] = math.log(self.num_messages['non_warning'] / n)
        self.word_counts['warning'] = {}
        self.word_counts['non_warning'] = {}

        for x, y in zip(X, Y):
            c = 'warning' if y == 1 else 'non_warning'
            counts = self.get_word_counts(self.tokenize(x))
            for word, count in counts.items():
                if word not in self.vocab:
                    self.vocab.add(word)
                if word not in self.word_counts[c]:
                    self.word_counts[c][word] = 0.0

                self.word_counts[c][word] += count

    def predict(self, X):

        result = []
        i = 0
        for x in X:
            counts = self.get_word_counts(self.tokenize(x))
            warning_score = 0
            nonwarning_score = 0
            for word, _ in counts.items():
                if word not in self.vocab: continue

                # add Laplace smoothing
                log_w_given_warning = math.log(
                    (self.word_counts['warning'].get(word, 0.0) + 1) / (self.num_messages['warning'] + len(self.vocab)))
                log_w_given_nonwarning = math.log((self.word_counts['non_warning'].get(word, 0.0) + 1) / (
                            self.num_messages['non_warning'] + len(self.vocab)))

                warning_score += log_w_given_warning
                nonwarning_score += log_w_given_nonwarning

            warning_score += self.log_class_priors['warning']
            nonwarning_score += self.log_class_priors['non_warning']
            predicted_class = 0
            if warning_score > nonwarning_score:
                predicted_class = 1
                result.append(predicted_class)
            else:
                predicted_class = 0
                result.append(predicted_class)
            i += 1
        return result