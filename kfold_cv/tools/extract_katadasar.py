from UBLTextMining import NeuralNetwork
import csv

nn = NeuralNetwork()
nn.load_stop_words('dataset\stopwords.txt')
# with open('lib\\katadasar.txt') as f:
#     corpus_indo = f.read().split()
training_data = []
with open('dataset\extract_katadasar_dataset.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        training_data.append(row[0])
len_training = len(training_data)
print(len_training)

i = 1
corpus_dari_training_data = []
for kalimat in training_data :
    check_kalimat = nn.preprocess_sentence_only(sentence=kalimat)
    for kata in check_kalimat :
        if kata not in corpus_dari_training_data:
            corpus_dari_training_data.append(kata)
    print('Loading {}%'.format(i/len_training*100))
    i= i+1

with open('dataset\\extracted_corpus.txt', 'w') as f:
    for item in corpus_dari_training_data:
        f.write("%s\n" % item)

print('Done')